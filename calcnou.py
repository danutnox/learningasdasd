def adunare(unu, doi):
    return unu + doi


def scadere(unu, doi):
    return unu - doi


def inmultire(unu, doi):
    return unu * doi


def impartire(unu, doi):
    return unu / doi


def alegerea():
    yy = 1
    while yy == 1:
        alegere = input("\nVreti sa faceti si alte calcule? y/n \n")
        if alegere.lower().strip() == 'y':
            tot()
        elif alegere.lower().strip() == 'n':
            print("Multumim ca ati calculat cu noi!\n")
            break
        else:
            print("Va rog alegeti din y sau n")


def tot():
    print("""In acest calculator veti putea face urmatoarele operatii algebrice:
    Adunare: +
    Scadere: -
    Inmultire: *
    Impartire: /
    Daca vreti sa stergeti si sa o luati de la capat, apasati tasta 'C'\n\n
    """)

    prim = ''
    secund = ''
    x = 1
    while x == 1:
        first = input('Introduceti primul numar natural: ')
        if first.strip().isdigit():
            prim = prim+first
            x = 0
        elif first.strip().lower() == 'c':
            return tot()
        else:
            print("\nVa rog introduceti un numar natural\n")
            continue

    while x == 0:
        second = input('Introduceti al 2-lea numar natural: \n')
        if second.strip().isdigit():
            secund = secund + second
            x = 1
        elif second.strip().lower() == 'c':
            return tot()
        else:
            print("Va rog introduceti un numar natural\n")
            continue

    while True:
        operatia = input('Introduceti semnul operatiei pe care doriti sa o realizati: \n')

        if operatia.strip() == '+':
            print('Rezultatul este ' + str(adunare(int(prim), int(secund))))
            alegerea()
            return False
        elif operatia.strip() == '-':
            print('Rezultatul este ' + str(scadere(int(prim), int(secund))))
            alegerea()
            return False
        elif operatia.strip() == '*':
            print('Rezultatul este ' + str(inmultire(int(prim), int(secund))))
            alegerea()
            return False
        elif operatia.strip() == '/':
            try:
                print('Rezultatul este ' + str(impartire(int(prim), int(secund))))
                alegerea()
                return False
            except ZeroDivisionError:
                print("Nu se poate efectua diviziunea cu 0\n")
                return tot()

        elif operatia.lower().strip() == 'c':
            return tot()
        else:
            print("Va rog introduceti un semn din cele 4 mentionate mai sus.\n")


if __name__ == '__main__':
    tot()
