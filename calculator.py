"""Cu ajutorul celor invatate in ultima saptamana la curs se va realiza un calculator, astfel:
- calculatorul detine cifre de la 0 la 9,
- semnele matematice cu ajutorul carora se vor putea realiza operatii matematice sunt urmatoarele: + (adunare), - (scadere), / (impartire), * (inmultire)
- pe langa cifrele amintite anterior si semnele matematice se va putea sterge cu ajutorul litere C
- toate operatiile presupun interactiunea cu utilizatorul (functii de tip input)

Mini-proiectul se va adauga pe GitLab si mi se va trimite linkul prin email.
Intrebarile le putem discuta tot prin email.

"""


def calculator():
    primul = int(input("Introduceti primul numar: ").strip())
    doilea = int(input("Introduceti al 2-lea numar: ").strip())
    operatia = input("Introduceti ce fel de operatie aritmetica doriti sa faceti ("
                     "adunare/scadere/impartire/inmultire): ").strip()

    if operatia == "+" or operatia == "adunare":
        print("Rezultatul este: " + str(primul + doilea))
    elif operatia == "-" or operatia == "scadere":
        print("Rezultatul este: " + str(primul - doilea))
    elif operatia == "/" or operatia == "impartire":
        print("Rezultatul este: " + str(primul / doilea))
    elif operatia == "*" or operatia == "inmultire":
        print("Rezultatul este: " + str(primul * doilea))
    elif operatia == "c":
        print("Ati sters si va trebui sa introduceti valorile din nou \n")
        calculator()
    else:
        print("Va rog sa introduceti un numar natural si o operatie aritmetica dintre cele mentionate mai sus.")
        calculator()


calculator()