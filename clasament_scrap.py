import requests
from bs4 import BeautifulSoup

r = requests.get('https://lpf.ro/')
url = BeautifulSoup(r.text, 'html.parser')

tabel = url.find('table', {'border': '0'})

f = open('clasament.html', 'w+', encoding='utf-8')
f.write("""<html><head><table><thead><tbody>""")

for x in tabel.findAll('tr'):
    f.write('''<tr>{}'''.format(x))
    for y in tabel.findAll('td')[0]:
        f.write('''<th>{}</th>'''.format(y))
    f.write("""</tr>""")

f.write("""</tbody></thead></table></head></html>""")
f.close()
