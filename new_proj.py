import requests
import mysql.connector

host = '127.0.0.1'
user = 'root'
database = 'test'

api = "https://jobs.github.com/positions.json"
subiect = input("Subiect de cautare: ")

# connect = mysql.connector.connect(user=user, host=host, database=database, password=user, use_pure=True)
# cursor = connect.cursor(buffered=True)

# cursor.execute("INSERT into location (country, city) VALUES ('romania', 'bucuresti')")
# cursor.execute("SELECT * FROM location")
# x = cursor.fetchall()
# for y in x:
#     print(y)


def get_jobs(sub):
    params = {"description": sub}
    n = requests.get(url=api, params=params)
    return n.json()


urls = []
titles = []
descriptions = []
location = []

for x in get_jobs(subiect):
    location.append(x['location'].split(',')[0])
    urls.append(x['url'])
    descriptions.append(x['description'])
    titles.append(x['title'])
    # cursor.execute("INSERT INTO location (city) VALUE {}".format(x['location'].split(',')[0])))
print(urls, titles, descriptions, location)