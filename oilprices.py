import requests
from bs4 import BeautifulSoup

r = requests.get('https://oilprice.com/oil-price-charts')
url = BeautifulSoup(r.text, 'html.parser')

tabel = url.find('table', {'class': 'oilprices__table'})
rows = url.find('tbody', {"class": 'row_holder'})

f = open('oilprices.html', 'w+', encoding='utf-8')
for x in url.findAll('span', {'class': 'blend_update_text'}):
    x.decompose()
f.write("<html><head></head><body><table><thead>")
for x in tabel.find_all('tr')[:1]:
    f.write("""<th>{}</th>""".format(x))
f.write("""</tr></thead><tbody>""")

for x in rows.findAll('tr'):
    f.write("""{}""".format(x))

f.write("""</tbody></table></body></html>""")
f.close()

