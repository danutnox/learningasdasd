# import re
# # if re.match(r'[A-Za-z0-9@#$%^&+=]{8,}', parola()):
# #     print("E buna")
# # else:
# #     print("Nu e buna")
# #     passcheck()
#
#
# def parola():
#     password = input("Enter string to test: ")
#     return password
#
#
# def passcheck():
#     if re.match(r'[A-Z]', parola()):
#         print("Parola este buna")
#     else:
#         print("Parola trebuie sa aiba intre 6 si 12 caractere.")
#         passcheck()
#
#
# passcheck()

import re


class PasswordChecker:
    def __init__(self, password, rules):
        self.password = password
        self.rules = rules

    def check(self):
        errors = []
        for k, v in self.rules.items():
            if not k(self.password):
                errors.append(v)
        return len(errors) == 0, errors


rules = {lambda x: 6 <= len(x) <= 12: "Parola trebuie sa aiba intre 6 si 12 caractere",
         lambda x: re.search(r'[a-z]+', x): "Parola trebuie sa contina cel putin o litera mica",
         lambda x: re.search(r'[A-Z]+', x): "Parola trebuie sa contina cel putin o litera mare",
         lambda x: re.search(r'[$#@]+', x): "Parola trebuie sa contina cel putin un caracter din $#@",
         lambda x: re.search(r'[0-9]+', x): "Parola trebuie sa contina cel putin o cifra"}

p = input("Introdu parola:\n")
res = PasswordChecker(p, rules).check()
if not res[0]:
    print('\n'.join(res[1]))
