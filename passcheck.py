import re


def parola():
    pas = input("Parola:\n").strip().split(',')
    for x in pas:
        print("Pentru parola {}: ".format(x))
        check(x)
        print("\n\n")


def check(pw):
    r1 = re.search(r'[a-z]+', pw)
    r2 = re.search(r'[A-Z]+', pw)
    r3 = re.search(r'[0-9]+', pw)
    r4 = re.search(r'[!@#$%^&*]+', pw)
    r5 = 6 <= len(pw) <= 12
    ceva = (r1, r2, r3, r4, r5)
    raspunsuri = ["Parola trebuie sa contina cel putin o litera mica.",
                  "Parola trebuie sa contina cel putin o litera mare.",
                  "Parola trebuie sa contina cel putin o cifra.",
                  "Parola trebuie sa contina cel putin un caracter special",
                  "Parola trebuie sa aiba cel putin 6 caractere si maxim 12"]

    for x, y in zip(ceva, raspunsuri):
        if not x:
            print(y)


parola()