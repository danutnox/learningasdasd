# Calculator realizat prin intermediul functiilor
# Definim conditiile primului numar
def primul():
    while True:
        nr1 = input("Introdu primul nr: ").strip()
        if nr1.upper() == "C":
            continue
        elif nr1.isnumeric():
            return int(nr1)
        else:
            print("Numarul introdus nu este valid!")

# Definim conditiile operatorului
def operator():
    while True:
        semn = input("Indrodu operatorul +, -, *, / : ").strip()
        acceptat = ("+", "-", "*", "/")
        if semn.upper() == "C":
            primul()
        elif semn in acceptat:
            return semn
        else:
            print("Operator invalid!")

# Definim conditiile celui de-al doilea numar
def al_doilea():
    while True:
        nr2 = input("Introdu a doilea nr: ").strip()
        if nr2.upper() == "C":
            operator()
        elif nr2.isnumeric():
            return int(nr2)
        else:
            print("Numarul introdus nu este valid!")

# Initiem calculatorul
while True:
    try:
        a = int(primul())
        o = operator()
        b = int(al_doilea())
        if o == "+":
            print(a + b)
        elif o == "-":
            print(a - b)
        elif o == "*":
            print(a + b)
        elif o == "/":
            print(a / b)
        else:
            print("Operator incorect!")
    except ZeroDivisionError:
        print("Nu puteti imparti la 0!")
