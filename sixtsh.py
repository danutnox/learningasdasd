import requests
from bs4 import BeautifulSoup


r = requests.get('https://www.sixt.ro/flota-autoturisme')
url = BeautifulSoup(r.text, 'html.parser')

titluri_masini = url.findAll("h3")
optiuni = url.findAll('div', {"class": 'sx-fleetlist-offerfeatures'})

#
f = open('list_masini.html', 'w+', encoding='utf-8')
f.write("<html><head></head><table><thead>")

for x in url.findAll('strong'):
    x.decompose()

for x in titluri_masini:
    f.write("\n<th>{}_________________</th>\n".format(x))
f.write("</thead><tbody>")

for x in optiuni:
    f.write("<td>{}</td>".format(x))

f.write("</tbody></table></html>")
f.close()




