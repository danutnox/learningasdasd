import random


optiuni = ['X', '0']
castigator1 = "X a castigat jocul."
castigator2 = "0 a castigat jocul."
print("""Salut!
Introdu cifra aferenta fiecarei casute in care vreti sa puneti X sau 0.
Bafta!""")


def board(tabla):
    x = tabla[0] + " | " + tabla[1] + " | " + tabla[2] + "\n" + tabla[3] + " | " + tabla[4] + " | " + tabla[5] + "\n" + \
        tabla[6] + " | " + tabla[7] + " | " + tabla[8]
    print(x)


def cineincepe():
    player1 = input("Introduceti numele primului jucator: ")
    player2 = input("Introduceti numele celui de-al doilea jucator: ")
    nr = random.randint(1, 2)
    if nr == 1:
        print("Incepe jucatorul cu numele: {}".format(player1.title()))
        return 1
    else:
        print("Incepe jucatorul cu numele: {}".format(player2.title()))
        return 2


def player1(tabla):
    try:
        loc = int(input("Precizati numarul casutei in care vreti sa puneti X: "))
        if tabla[loc-1] == "_" and 0 <= loc <= 9:
            tabla[loc-1] = "X"
            board(tabla)
        else:
            print("Va rog introduceti cifra unei casute libere")
            player1(tabla)
    except Exception:
        print("Va rog introduceti o cifra de la 1 la 9")
        player1(tabla)


def player2(tabla):
    try:
        loc = int(input("Precizati numarul casutei in care vreti sa puneti 0: "))
        if tabla[loc-1] == "_" and 0 < loc <= 9:
            tabla[loc-1] = "0"
            board(tabla)

        else:
            print("Va rog introduceti cifra unei casute libere")
            player2(tabla)
    except Exception:
        print("Va rog introduceti o cifra de la 1 la 9")
        player2(tabla)


def dinnou():
    iar = input("Doriti sa jucati din nou? y/n")
    if iar.lower() == 'y':
        joaca()
    elif iar.lower() == 'n':
        pass
    else:
        print("Va rog alegeri y sau n.")
        dinnou()


def remiza(tabla):
    for x in tabla:
        if '_' not in tabla:
            print("Remiza!!!!!!!")
            dinnou()
        else:
            pass

def joaca():
    tabla = ["_", "_", "_", "_", "_", "_", "_", "_", "_"]
    board(tabla)
    cineincepe()

    while True:
        player1(tabla)
        if tabla[0] == "X" and tabla[1] == "X" and tabla[2] == "X":
            print(castigator1)
            return False
        elif tabla[3] == "X" and tabla[4] == "X" and tabla[5] == "X":
            print(castigator1)
            return False
        elif tabla[6] == "X" and tabla[7] == "X" and tabla[8] == "X":
            print(castigator1)
            return False
        elif tabla[0] == "X" and tabla[3] == "X" and tabla[6] == "X":
            print(castigator1)
            return False
        elif tabla[2] == "X" and tabla[5] == "X" and tabla[8] == "X":
            print(castigator1)
            return False
        elif tabla[0] == "X" and tabla[4] == "X" and tabla[8] == "X":
            print(castigator1)
            return False
        elif tabla[2] == "X" and tabla[4] == "X" and tabla[6] == "X":
            print(castigator1)
            return False
        elif "_" not in tabla:
            print("REMIZAAAAAA")
            dinnou()
        else:
            pass

        player2(tabla)
        if tabla[0] == "0" and tabla[1] == "0" and tabla[2] == "0":
            print(castigator2)
            return False
        elif tabla[3] == "0" and tabla[4] == "0" and tabla[5] == "0":
            print(castigator2)
            return False
        elif tabla[6] == "0" and tabla[7] == "0" and tabla[8] == "0":
            print(castigator2)
            return False
        elif tabla[0] == "0" and tabla[3] == "0" and tabla[6] == "0":
            print(castigator2)
            return False
        elif tabla[2] == "0" and tabla[5] == "0" and tabla[8] == "0":
            print(castigator2)
            return False
        elif tabla[0] == "0" and tabla[4] == "0" and tabla[8] == "0":
            print(castigator2)
            return False
        elif tabla[2] == "0" and tabla[4] == "0" and tabla[6] == "0":
            print(castigator2)
            return False
        elif "_" not in tabla:
            print("REMIZAAAAAA")
            dinnou()
        else:
            continue


joaca()
dinnou()